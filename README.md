This document has been prepared by Alper Gulec only for the trendyol backend case study information

Used Tools
============

Wiremock
Rest Assured
Junit


Installation
============

Service virtualization has been done using wiremock.
Different responses are returned for each scenario.
Before starting the test scenarios, wiremock is running on localhost: 8080.