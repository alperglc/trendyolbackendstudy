package utility;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import model.Book;
import response.ErrorResponse;
import response.Messages;

public class Utilty implements Messages {

	private Gson gson = new Gson();

	public JsonObject generateRequestBody(String title, String author) {
		Book book = new Book();
		book.setTitle(title);
		book.setAuthor(author);
		return (JsonObject) gson.toJsonTree(book);
	}

	public String removeFieldFromJSON(String field) {
		JsonObject element = generateRequestBody(EXAMPLE_TITLE_VALUE, EXAMPLE_AUTHOR_VALUE);
		element.remove(field);
		return String.valueOf(element);
	}

	public JsonObject generateRequiredResponse(String whichField) {
		ErrorResponse errorResponse = new ErrorResponse();
		if (whichField.equals(TITLE)) {
			errorResponse.setError(TITLE_FIELD_REQUIRED_ERROR_MESSAGE);

		} else if (whichField.equals(AUTHOR)) {
			errorResponse.setError(AUTHOR_FIELD_REQUIRED_ERROR_MESSAGE);
		}
		return (JsonObject) gson.toJsonTree(errorResponse);
	}

	public JsonObject generateEmptyResponse(String whichField) {
		ErrorResponse errorResponse = new ErrorResponse();
		if (whichField.equals(TITLE)) {
			errorResponse.setError(TITLE_FIELD_EMPTY_ERROR_MESSAGE);

		} else if (whichField.equals(AUTHOR)) {
			errorResponse.setError(AUTHOR_FIELD_EMPTY_ERROR_MESSAGE);
		}
		return (JsonObject) gson.toJsonTree(errorResponse);
	}

	public JsonObject generateRequestBodyWithId(int id, String title, String author) {
		Book book = new Book();
		book.setId(id);
		book.setTitle(title);
		book.setAuthor(author);
		return (JsonObject) gson.toJsonTree(book);
	}

	public JsonElement generateIdFieldReadOnlyResponse() {
		ErrorResponse errorResponse = new ErrorResponse();
		errorResponse.setError(ID_FIELD_READ_ONLY_ERROR_MESSAGE);
		return gson.toJsonTree(errorResponse);
	}

	public JsonElement generateCanNotCreateDuplicateResponse() {
		ErrorResponse errorResponse = new ErrorResponse();
		errorResponse.setError(EXIST_BOOK_ERROR_MESSAGE);
		return gson.toJsonTree(errorResponse);
	}

	public JsonElement createdNewBookResponse(int id, String title, String author) {
		Book book = new Book();
		book.setId(id);
		book.setTitle(title);
		book.setAuthor(author);
		return gson.toJsonTree(book);
	}

}
