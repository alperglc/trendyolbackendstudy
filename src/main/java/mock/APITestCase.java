package mock;

import com.github.tomakehurst.wiremock.WireMockServer;
import config.TestConfig;
import groovy.util.logging.Slf4j;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.junit.Assert;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import response.Messages;
import utility.Utilty;

import java.util.Objects;

import static com.github.tomakehurst.wiremock.client.WireMock.*;

@Slf4j
public class APITestCase implements Messages{

	public static String API_ROOT = Objects.requireNonNull(TestConfig.getProperty("API_ROOT_URL"));
	public static String BASE_PATH = Objects.requireNonNull(TestConfig.getProperty("BASE_PATH"));
	public static final Logger log = LoggerFactory.getLogger(APITestCase.class);
	Utilty utilty = new Utilty();

	WireMockServer wireMockServer;

	@BeforeEach
	public void setup() {
		wireMockServer = new WireMockServer(8080);
		wireMockServer.start();
		log.info("Mocking server started via localhost:8080");
	}

	@AfterEach
	public void teardown() {
		wireMockServer.stop();
	}

	public RequestSpecification requestSpecification() {
		RestAssured.baseURI = API_ROOT;
		RequestSpecification requestSpecification = RestAssured.given().urlEncodingEnabled(false);
		requestSpecification.contentType(ContentType.JSON);
		return requestSpecification;
	}

	public void assertResponseCode(Response response, int i) {
		int statusCode = response.getStatusCode();
		Assert.assertEquals(i, statusCode);
	}
	public void mockNoBookResponse() {
		stubFor(get(urlEqualTo(BASE_PATH)).willReturn(aResponse().withStatus(200).withBody("{}").withHeader(CONTENT_TYPE, JSON)));
	}

	public void requiredFieldResponse(String whichField) {
		String reqBody = utilty.removeFieldFromJSON(whichField);
		String response = String.valueOf(utilty.generateRequiredResponse(whichField));
		if (whichField.equals(TITLE)) {
			stubFor(post(urlEqualTo(BASE_PATH)).withRequestBody(equalToJson(reqBody))
					.willReturn(aResponse().withStatus(400).withBody(response).withHeader(CONTENT_TYPE, JSON)));

		} else if (whichField.equals(AUTHOR)) {
			stubFor(post(urlEqualTo(BASE_PATH)).withRequestBody(equalToJson(reqBody))
					.willReturn(aResponse().withStatus(400).withBody(response).withHeader(CONTENT_TYPE, JSON)));
		}

	}

	public void emptyFieldResponse(String whichField) {
		String response = String.valueOf(utilty.generateEmptyResponse(whichField));
		if (whichField.equals(TITLE)) {
			String reqBody = String.valueOf(utilty.generateRequestBody("", EXAMPLE_AUTHOR_VALUE));
			stubFor(post(urlEqualTo(BASE_PATH)).withRequestBody(equalToJson(reqBody))
					.willReturn(aResponse().withStatus(400).withBody(response).withHeader(CONTENT_TYPE, JSON)));

		} else if (whichField.equals(AUTHOR)) {
			String reqBody = String.valueOf(utilty.generateRequestBody(EXAMPLE_TITLE_VALUE, ""));
			stubFor(post(urlEqualTo(BASE_PATH)).withRequestBody(equalToJson(reqBody))
					.willReturn(aResponse().withStatus(400).withBody(response).withHeader(CONTENT_TYPE, JSON)));
		}
	}

	public void idIsReadOnly() {
		String response = String.valueOf(utilty.generateIdFieldReadOnlyResponse());
		String reqBody = String.valueOf(utilty.generateRequestBodyWithId(1, EXAMPLE_TITLE_VALUE, EXAMPLE_AUTHOR_VALUE));
		stubFor(post(urlEqualTo(BASE_PATH+"/1")).withRequestBody(equalToJson(reqBody))
				.willReturn(aResponse().withStatus(400).withBody(response).withHeader(CONTENT_TYPE, JSON)));

	}

	public void listBooks(String type) {
		if (type.equals("create")) {
			String reqBody = String.valueOf(utilty.generateRequestBodyWithId(2, EXAMPLE_TITLE_VALUE, EXAMPLE_AUTHOR_VALUE));
			stubFor(post(urlEqualTo(BASE_PATH+"/2")).withRequestBody(equalToJson(reqBody)).willReturn(aResponse().withStatus(201).withHeader(CONTENT_TYPE, JSON)));

		} else if (type.equals("get")) {
			String response = String.valueOf(utilty.createdNewBookResponse(2, EXAMPLE_TITLE_VALUE, EXAMPLE_AUTHOR_VALUE));
			stubFor(get(urlEqualTo(BASE_PATH+"/2")).willReturn(aResponse().withStatus(200).withBody(response).withHeader(CONTENT_TYPE, JSON)));

		}
	}

	public void canNotCreateDuplicate() {
		String response = String.valueOf(utilty.generateCanNotCreateDuplicateResponse());
		String reqBody = String.valueOf(utilty.generateRequestBodyWithId(2, EXAMPLE_TITLE_VALUE, EXAMPLE_AUTHOR_VALUE));
		stubFor(post(urlEqualTo(BASE_PATH+"/3")).withRequestBody(equalToJson(reqBody))
				.willReturn(aResponse().withStatus(400).withBody(response).withHeader(CONTENT_TYPE, JSON)));

	}

	public void assertErrorMessage(Response response, String whichField, String responseMessage){

		Assert.assertEquals(response.getBody().jsonPath().get(whichField),responseMessage);

	}
}
