package mock;

import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.junit.Assert;
import org.junit.jupiter.api.Test;
import response.Messages;
import utility.Utilty;

public class TestCases extends APITestCase implements Messages {

	RequestSpecification requestSpecification = requestSpecification();
	Utilty utilty = new Utilty();

	@Test
	public void noBookStoredOnTheServer() {
		mockNoBookResponse();
		Response response = requestSpecification.get(BASE_PATH);
		assertResponseCode(response, 200);
		Assert.assertEquals("{}", response.getBody().asString());
	}

	@Test
	public void titleMustBeRequiredField() {
		requiredFieldResponse(TITLE);
		requestSpecification.body(utilty.removeFieldFromJSON(TITLE));
		Response response = requestSpecification.post(BASE_PATH);
		assertResponseCode(response, 400);
		assertErrorMessage(response, ERROR, TITLE_FIELD_REQUIRED_ERROR_MESSAGE);

	}
	@Test
	public void authorMustBeRequiredField() {
		requiredFieldResponse(AUTHOR);
		requestSpecification.body(utilty.removeFieldFromJSON(AUTHOR));
		Response response = requestSpecification.post(BASE_PATH);
		assertResponseCode(response, 400);
		assertErrorMessage(response,ERROR,AUTHOR_FIELD_REQUIRED_ERROR_MESSAGE);
	}

	@Test
	public void authorMustNotBeEmpty() {
		emptyFieldResponse(AUTHOR);
		String requestBody = String.valueOf(utilty.generateRequestBody(EXAMPLE_TITLE_VALUE, ""));
		requestSpecification.body(requestBody);
		Response response = requestSpecification.post(BASE_PATH);
		assertResponseCode(response, 400);
		assertErrorMessage(response,ERROR,AUTHOR_FIELD_EMPTY_ERROR_MESSAGE);

	}

	@Test
	public void titleMustNotBeEmpty() {
		emptyFieldResponse(TITLE);
		String requestBody = String.valueOf(utilty.generateRequestBody("", EXAMPLE_AUTHOR_VALUE));
		requestSpecification.body(requestBody);
		Response response = requestSpecification.post(BASE_PATH);
		assertResponseCode(response, 400);
		assertErrorMessage(response,ERROR,TITLE_FIELD_EMPTY_ERROR_MESSAGE);

	}

	@Test
	public void verifyIdIsReadOnlyForPutRequest() {
		idIsReadOnly();
		String requestBody = String.valueOf(utilty.generateRequestBodyWithId(1, EXAMPLE_TITLE_VALUE, Messages.EXAMPLE_AUTHOR_VALUE));
		requestSpecification.body(requestBody);
		Response response = requestSpecification.post(BASE_PATH+"/1");
		assertResponseCode(response, 400);
		assertErrorMessage(response,ERROR,ID_FIELD_READ_ONLY_ERROR_MESSAGE);
	}

	@Test
	public void createANewBookAndGetCreatedBook() {
		listBooks("create");
		String requestBody = String.valueOf(utilty.generateRequestBodyWithId(2, EXAMPLE_TITLE_VALUE, EXAMPLE_AUTHOR_VALUE));
		requestSpecification.body(requestBody);
		Response response = requestSpecification.post(BASE_PATH+"/2");
		assertResponseCode(response, 201);

		listBooks("get");
		Response response1 = requestSpecification.get(BASE_PATH+"/2");
		assertResponseCode(response1, 200);
		Assert.assertEquals(response1.getBody().jsonPath().get(ID), 2);
		assertErrorMessage(response1,TITLE,EXAMPLE_TITLE_VALUE);
		assertErrorMessage(response1,AUTHOR,EXAMPLE_AUTHOR_VALUE);
	}

	@Test
	public void cannotCreateDuplicateBook() {
		listBooks("create");
		String requestBody = String.valueOf(utilty.generateRequestBodyWithId(2, EXAMPLE_TITLE_VALUE, EXAMPLE_AUTHOR_VALUE));
		requestSpecification.body(requestBody);
		Response response = requestSpecification.post(BASE_PATH+"/2");
		assertResponseCode(response, 201);
		canNotCreateDuplicate();

		requestSpecification.body(requestBody);
		Response response1 = requestSpecification.post(BASE_PATH+"/3");
		assertResponseCode(response1, 400);
		assertErrorMessage(response1,ERROR,EXIST_BOOK_ERROR_MESSAGE);

	}

}
