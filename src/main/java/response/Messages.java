package response;

public interface Messages {

	String TITLE_FIELD_REQUIRED_ERROR_MESSAGE = "title field must be mandatory";
	String AUTHOR_FIELD_REQUIRED_ERROR_MESSAGE = "author field must be mandatory";
	String TITLE_FIELD_EMPTY_ERROR_MESSAGE = "title field must not be empty";
	String AUTHOR_FIELD_EMPTY_ERROR_MESSAGE = "author field must not be empty";
	String TITLE = "title";
	String AUTHOR = "author";
	String ID_FIELD_READ_ONLY_ERROR_MESSAGE = "id field is read-only";
	String EXIST_BOOK_ERROR_MESSAGE = "Another book with similar title and author already exists.";
	String EXAMPLE_TITLE_VALUE = "ExampleTitle";
	String EXAMPLE_AUTHOR_VALUE = "ExampleAuthor";
	String CONTENT_TYPE = "Content-Type";
	String JSON = "application/json";
	String ERROR = "error";
	String ID = "id";
}
